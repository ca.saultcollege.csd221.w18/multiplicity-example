/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ca.saultcollege.csd221.w18.rodney_martin.multiplicityexample;

/**
 *
 * @author rod
 */
public class Trinket {
    
    Hand whichHandIAmIn;
    
    public void putInHand(Hand hand) {
        this.whichHandIAmIn = hand;
    }
    
    public boolean isInAHand() {
        // If whichHandIAmIn is null, then this Trinket is not in a hand
        // otherwise it is
        return whichHandIAmIn != null;
    }
    
}
