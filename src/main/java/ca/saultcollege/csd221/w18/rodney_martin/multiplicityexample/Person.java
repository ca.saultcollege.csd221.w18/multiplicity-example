/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ca.saultcollege.csd221.w18.rodney_martin.multiplicityexample;

/**
 *
 * @author rod
 */
public class Person {
    
    private final Hand leftHand;
    private final Hand rightHand;
    
    public Person() {
        this.leftHand = new Hand();
        this.rightHand = new Hand();
    }
    
    public Hand getLeftHand() { return leftHand; }
    public Hand getRightHand() { return rightHand; }
}
