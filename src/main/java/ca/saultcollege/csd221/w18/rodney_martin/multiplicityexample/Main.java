/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ca.saultcollege.csd221.w18.rodney_martin.multiplicityexample;

/**
 * 
 * This Program is *one possible* solution to the UML diagram in
 * src/main/resources/UML-multiplicity.png
 * 
 * NOTE: in class, I has presented a solution that used a PersonFactory
 * to build a Person object for us. I have left that code in here, but
 * commented it out because it doesn't in fact follow the UML spec, which
 * indicates that Hand should be a composition in Person.  By using a 
 * Factory, I was giving Hand more of an Aggregation relationship because
 * each Hand object could technically live on beyond the Person to which it 
 * it belonged.
 * 
 * This version uses a Factory to build a person, and a Giver
 * class whose responsibility it is to put Trinkets into hands
 * and make sure Trinkets are in only one hand at a time.  The Giver
 * does this by asking the Trinket whether it is already in a hand or not,
 * and throwing an exception if it is.
 *
 * @author rod
 */
public class Main {
    
    public static void main(String[] args) {
        
        Person person1 = new Person();
        Person person2 = new Person();
        
        Giver giver = new Giver();
        
        Trinket trinket1 = new Trinket();
        Trinket trinket2 = new Trinket();
        
        try {
            
            // Add 2 trinkets to person1's left hand
            giver.give(person1.getLeftHand(), trinket1);
            giver.give(person1.getLeftHand(), trinket2);
            
            // This line should throw an exception because
            // trinket1 is already in a hand
            //giver.give(person2.getLeftHand(), trinket1);
            
            System.out.println("Success!");
        } catch ( Exception e ) {
            System.out.println(e.getMessage());
        }
        
    }
}
