/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ca.saultcollege.csd221.w18.rodney_martin.multiplicityexample;

//import java.util.Map;
//import java.util.HashMap;

/**
 * Giver is a class that is responsible for putting Trinkets into Hands
 * and making sure that a Trinket doesn't go into more than one Hand at
 * a time.
 * 
 * @author rod
 */
public class Giver {
        
    public void give(Hand hand, Trinket theTrinketToAdd) throws Exception {
        
        // Check if thing is already in a hand
        if ( theTrinketToAdd.isInAHand() ) {
            throw new Exception("Thing " + theTrinketToAdd + " is already in a hand");
        } else {
            theTrinketToAdd.putInHand(hand);
            hand.addTrinket(theTrinketToAdd);
        }
    }
    
}
