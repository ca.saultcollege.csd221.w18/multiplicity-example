/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ca.saultcollege.csd221.w18.rodney_martin.multiplicityexample;

import java.util.List;
import java.util.ArrayList;

/**
 *
 * @author rod
 */
public class Hand {
    
    private List<Trinket> trinketsInThisHand;
    
    public Hand() {
        // ArrayList is a concrete implementation of List interface
        this.trinketsInThisHand = new ArrayList<>();
    }
    
    public void addTrinket(Trinket theTrinketToAdd) {
        this.trinketsInThisHand.add(theTrinketToAdd);
    }
}
